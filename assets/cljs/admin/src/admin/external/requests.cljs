(ns admin.external.requests
  (:require
   [reagent.core :as r]
   [cljs.core.async :refer [go]]
   [cljs.core.async.interop :refer [<p!]]
   [admin.external.response-broker :refer [send]]
   [admin.external.env :refer [config]]
   )
  )


; In development mode, these timeout and then succeed.
; In test mode, these make requests to the specified server.

(defn timeout [val]
  ( js/Promise. (fn [resolve reject]
                  (js/setTimeout
                   (fn []
                     (js/console.log "timing out, then succeeding")
                     (resolve val)
                     )
                   2000)
                  )
   )
  )

(defn timeout-then-succeed
  ( [] (timeout nil))
  ([val] (timeout val)
   )
  )


(defn request! []
  (send :submitted nil)
  (timeout-then-succeed)
  )

(def server (:server config))


; resolves to true/false depending whether the delete succeeded.
(defn fetch-nothing [url opts ]
  (js/Promise. (fn [resolve reject ]
                   (go
                     (try 
                        (let [req (js/fetch url opts)
                             res (<p! req)
                             ]
                         (resolve true)
                         )
                       (catch :default e
                         (send :error e)
                         (resolve false)
                         )
                       )
                     )
                 ))
  )

(defn fetch-json [url opts ]
  (js/Promise. (fn [resolve reject]
                   (go
                     (try 
                       (let [req (js/fetch url opts)
                             res (<p! req)
                             json (js->clj (<p! (.json res)) :keywordize-keys true)
                             ]
                         (resolve json)
                         )
                       (catch :default e
                         (send :error e)
                         (resolve nil)
                         )
                       )
                     )
                 ))
  )

(defn dev-server []
  (= server "")
  )

(defn full-url [path]
  (str server path)
  )

(defn do-get [path ]
  (if (dev-server)
    (timeout-then-succeed)
    (fetch-json (full-url path) #js { "method" "GET" } )
    )
  )

(defn do-post [path form-body ]
  (if (dev-server)
    (timeout-then-succeed)
    (fetch-json (full-url path) #js { "method" "POST", "body" form-body} )
    )
  )

(defn do-delete [path ]
  (if (dev-server)
    (timeout-then-succeed true)
    (fetch-nothing (full-url path) #js { "method" "DELETE" } )
    )
  )

(defn do-patch [path form-body ]
  (if (dev-server)
    (do
     (timeout-then-succeed))
    (fetch-json (full-url path) #js { "method" "PATCH", "body" form-body} )
    )
  )
