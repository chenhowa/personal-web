(ns admin.external.response-broker-process
  (:require
   [admin.global-state.skills :refer [global-skills]]
   [admin.global-state.posts :refer [global-posts]]
   [admin.global-state.projects :refer [global-projects]]
   [admin.global-state.request :as req]
   [admin.global-state.page :as page]
   [cljs.core.async :refer [take!]]
   [admin.external.response-broker :refer [broker-chan send]]
   )
  )

(defn replace-id [seq search-id replacement]
  (let [f (fn [{:keys [id] :as item}]
            (if (= id search-id)
              replacement
              item
              )
            )
        ]
    (map f seq)
    )
  )

; Loop that takes something off the channel and processes it
(defn- process []
  (js/console.log "processing")
  (take! broker-chan
         (fn [[topic data]]
           (js/console.log (str "topic: " topic))
           (js/console.log data)
           (case topic
             :new-skill (swap! global-skills #(conj % data))
             :edit-skill (swap! global-skills
                                (fn [skills]
                                  (into [] (replace-id skills (:id data) data))
                                  ))
             :delete-skill (swap! global-skills
                              (fn [skills]
                                (into []
                                      (filter #(not= data (:id %)) skills))
                                ))
             :new-post (swap! global-posts #(conj % data))
             :edit-post (identity 1)
             :new-project (swap! global-projects #(conj % data))
             :change-page (page/change-page data)
             :submitted (req/submitted)
             :success (req/success)
             :error (do
                      (js/console.log "error PROCESSING")
                      (identity 1))
             (do
               (js/console.log "fail PROCESSING")
               (send :error (str "Unknown event with tag: " (str topic) (str data)))
               )
             )
           (trampoline process)
           )
         )
  )

; immediately start the process of message processing
(defn start-events []
  (process)
  )
