(ns admin.global-state.page
  (:require
   [reagent.core :as r]
   [reagent.dom :as d]
   )
  )


(def example-page-state
  { :page-id "root"
    :page-state "modal" ; or "normal"
    :scroll-position 55
   }
  )

(def previous-page-state (r/atom nil))

(def page-state (r/atom nil))

(defn change-page [page-id]
  (do
    (reset! previous-page-state @page-state)
    (reset! page-state {:id page-id
                        :page-state "normal"
                        :scroll-pos 0
                        })
    )
  )


(defn reset-current-page []
  (swap! page-state (fn [state]
                      (assoc state
                             :page-state "normal"
                             :scroll-pos 0
                             )
                      ))
  )

(defn open-modal []
  (swap! page-state (fn [state]
                      (assoc state
                             :page-state "modal"
                             )
                      ))
  )

(defn close-modal []
  (swap! page-state (fn [state]
                      (assoc state
                             :page-state "normal"
                             )
                      ))
  )
