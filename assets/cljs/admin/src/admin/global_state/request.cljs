(ns admin.global-state.request
  (:require
   [reagent.core :as r]
   )
  )


(def request-status (r/atom :ready)) ; ready, submitting finished

(defn- change [atom next time]
  (js/setTimeout #(reset! atom next) 200)
  )

(defn submitted []
  (reset! request-status :submitted)
  )

(defn success []
  (do 
    (reset! request-status :finished)
    (change request-status :ready 300))
  )
