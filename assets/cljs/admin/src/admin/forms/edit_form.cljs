(ns admin.forms.edit-form
  (:require
   [component-lib.core :as c]
   [fork.reagent :as f]
   [clojure.string :as str]
   [admin.external.requests :as request :refer [request!]]
   [admin.external.response-broker :refer [send]]
   [admin.global-state.request :refer [request-status]]
   [admin.external.side-effect :refer [side-effect]]
   [admin.external.utils :refer [form-data]]
   [admin.direction :refer [go-back!]]
   [cljs.core.async :refer [go]]
   [cljs.core.async.interop :refer [<p!]]
   ))


(defn blog-form []
  (let [
        form-status request-status
        ]
    (fn [{:keys [values
                 set-values
                 handle-change
                 handle-submit
                 submitting?
                 form-id
                 props
                 ]}] 
      [:form { :id form-id
              :class "Blog-Form"
              :on-submit handle-submit
              }
       [side-effect {:is-true (= :finished @form-status)
                     :on-true #((:back! props))
                     }]
       [c/input-group {:label "Title",
                       :type "text"
                       :id "title"
                       :on-change handle-change
                       :value (values "title")
                       }]
       [c/input-group {:label "Subtitle",
                       :type "text"
                       :id "subtitle"
                       :on-change handle-change
                       :value (values "subtitle")
                       }]
       [c/input-group {:label "Date",
                       :type "date"
                       :id "edit-date"
                       :on-change handle-change
                       :value (values "edit-date")
                       }]
       [c/markdown-editor {:id "blog-markdown-editor"
                           :on-change #(set-values {"content" %})
                           }]

       [c/submit-button {
                         :disabled (not= @form-status :ready)
                         :class "Blog-Submit"
                         } "submit"]
       ]))
  )

(defn- submit-edit-post [vals id]
  (go
    (send :submitted nil)
    (send :edit-post
          (<p! (request/do-patch (str "/admin/api/posts/" id)
                                 (form-data vals)))
          )
    (send :success nil)
    )
  )

(defn render-blog-form [id]
  [f/form {:prevent-default? true
           :clean-on-unmount? true
           :initial-values
           { "edit-date" (-> (js/Date.) (.toISOString) (str/split #"T") (first))
            
            }
           :path :login-form 
           :on-submit #(request!)
           :props {:back! go-back!}
           } blog-form
   ]
  )

(defn- submit-new-post [vals]
  (go
     (send :submitted nil)
     (send :new-post
           (<p! (request/do-post "/admin/api/posts" (form-data vals)))
           )
     (send :success nil)
    )
  )


(defn render-new-form []
  [f/form {:prevent-default? true
           :clean-on-unmount? true
           :initial-values
           { "edit-date" (-> (js/Date.) (.toISOString) (str/split #"T") (first))
             "title" ""
             "subtitle" ""
             "content" ""
            }
           :path :login-form 
           :on-submit (fn [{:keys [state values]}]
                        (js/console.log values)
                        (submit-new-post values)
                        )
           :props {:back! go-back!
                   
                   }
           } blog-form
   ]
  )
